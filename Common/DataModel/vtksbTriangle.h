/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtksbTriangle.h

  Copyright (c) 2014 The Johns Hopkins University Applied Physics Laboratory
  All rights reserved.

=========================================================================*/
// .NAME vtksbTriangle - a cell that represents a triangle
// .SECTION Description
// The purpose of this class is to allow the IntersectWithLine
// function to be called from Java. The problem is that the default
// IntersectWithLine function takes references which the Java Wrapper
// can't handle. All references have therefore been replaced with 1 element
// arrays in the method signature.

#ifndef __vtksbTriangle_h
#define __vtksbTriangle_h

#include "vtkTriangle.h"

class VTKCOMMONDATAMODEL_EXPORT vtksbTriangle : public vtkTriangle
{
public:
  static vtksbTriangle *New();
  vtkTypeMacro(vtksbTriangle,vtkTriangle);

  // Description:
  // This simply calls the corresponding method in the base class but
  // allows this function to be called from Java since all references
  // have been replaced with 1 element arrays.
  int IntersectWithLine(double p1[3], double p2[3], double tol, double t[1],
                        double x[3], double pcoords[3], int subId[1])
  {
    return Superclass::IntersectWithLine(p1, p2, tol, t[0], x, pcoords, subId[0]);
  }

protected:
  vtksbTriangle() {};
  ~vtksbTriangle() {};

private:
  vtksbTriangle(const vtksbTriangle&);  // Not implemented.
  void operator=(const vtksbTriangle&);  // Not implemented.
};

#endif


