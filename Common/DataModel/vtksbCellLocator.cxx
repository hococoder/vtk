/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtksbCellLocator.cxx

  Copyright (c) 2010 The Johns Hopkins University Applied Physics Laboratory
  All rights reserved.

=========================================================================*/
#include "vtksbCellLocator.h"
#include "vtkObjectFactory.h"

//----------------------------------------------------------------------------

vtkStandardNewMacro(vtksbCellLocator);
