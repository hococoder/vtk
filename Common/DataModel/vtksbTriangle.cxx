/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtksbTriangle.cxx

  Copyright (c) 2014 The Johns Hopkins University Applied Physics Laboratory
  All rights reserved.

=========================================================================*/
#include "vtksbTriangle.h"
#include "vtkObjectFactory.h"

vtkStandardNewMacro(vtksbTriangle);
