/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtksbModifiedBSPTree.cxx

  Copyright (c) 2010 The Johns Hopkins University Applied Physics Laboratory
  All rights reserved.

=========================================================================*/
#include "vtksbModifiedBSPTree.h"
#include "vtkObjectFactory.h"

//----------------------------------------------------------------------------

vtkStandardNewMacro(vtksbModifiedBSPTree);
