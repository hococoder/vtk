/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtksbModifiedBSPTree.h

  Copyright (c) 2010 The Johns Hopkins University Applied Physics Laboratory
  All rights reserved.

=========================================================================*/
// .NAME vtksbModifiedBSPTree - ModifiedBSPTree class for vtk
// .SECTION Description
// The purpose of this class is to allow the IntersectWithLine
// function to be called from Java. The problem is that the default
// IntersectWithLine function takes references which the Java Wrapper
// can't handle. All references have therefore been replaced with 1 element
// arrays in the method signature.

#ifndef __vtksbModifiedBSPTree_h
#define __vtksbModifiedBSPTree_h

#include "vtkModifiedBSPTree.h"

class VTKFILTERSFLOWPATHS_EXPORT vtksbModifiedBSPTree : public vtkModifiedBSPTree
{
public:
  static vtksbModifiedBSPTree *New();
  vtkTypeMacro(vtksbModifiedBSPTree,vtkModifiedBSPTree);

  // Description:
  // This simply calls the corresponding method in the base class but
  // allows this function to be called from Java since all references
  // have been replaced with 1 element arrays.
  int IntersectWithLine(double a0[3], double a1[3], double tol,
                        double t[1], double x[3], double pcoords[3],
                        int subId[1], vtkIdType cellId[1],
                        vtkGenericCell *cell)
  {
      return Superclass::IntersectWithLine(a0, a1, tol, t[0], x, pcoords, subId[0], cellId[0], cell);
  }

  // Description:
  // This simply calls the corresponding method in the base class but
  // allows this function to be called from Java since all pointers
  // have been replaced with 1 element arrays.
  // Note this function only works with polydata with cells that have
  // no more than 3 vertices (since weights is specified with 3 elements).
  vtkIdType FindCell(double x[3], double tol2, vtkGenericCell *GenCell,
                     double pcoords[3], double weights[3])
  {
	  return Superclass::FindCell(x, tol2, GenCell, pcoords, weights);
  }

protected:
  vtksbModifiedBSPTree() {};
  ~vtksbModifiedBSPTree() {};
private:
  vtksbModifiedBSPTree(const vtksbModifiedBSPTree&);  // Not implemented.
  void operator=(const vtksbModifiedBSPTree&);  // Not implemented.
};

#endif 
