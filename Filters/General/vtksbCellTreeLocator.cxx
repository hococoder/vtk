/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtksbCellTreeLocator.cxx

  Copyright (c) 2010 The Johns Hopkins University Applied Physics Laboratory
  All rights reserved.

=========================================================================*/
#include "vtksbCellTreeLocator.h"
#include "vtkObjectFactory.h"

//----------------------------------------------------------------------------

vtkStandardNewMacro(vtksbCellTreeLocator);
